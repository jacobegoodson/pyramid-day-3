/*
    PROBLEM STATEMENT
        Write a program to generate the following pattern based on the int n being given to numberPatternNthRow
        1
        2 2
        4 4 4 4
        5 5 5 5 5
        7 7 7 7 7 7 7
        … n

    Example of main method:
        <Program Start>
            Enter a number to generate a pattern:
            4
            -- PATTERN --
            1
            2 2
            4 4 4 4
        <Program Finish>
*/

public class NumberPattern {
    // this method should simply generate a row based int n
    // example:
    // numberPatternNthRow(2) => 2 2
    // numberPatternNthRow(3) =>
    // numberPatternNthRow(5) => 5 5 5 5 5
    public String numberPatternNthRow(int n) {
        // your code goes here
        return "";
    }

    public void numberPatternPrinter(int n){
        // This should print the number pattern according to the specification
        // by calling numberPattern nthRow repeatedly
        // example:
        /*
            numberPatternPrinter(5) =>
                                        1
                                        2 2
                                        4 4 4 4
                                        5 5 5 5 5
         */
        // your code goes here

    }

    public static void main(String[] args) {
        // place your final code here
    }
}
