import net.jqwik.api.*;
import org.junit.jupiter.api.Assertions;

import java.util.HashMap;
import java.util.function.Function;

class CalculateCircleTest {
    private CalculateCircleData calculateCircleData = new CalculateCircleData();

    private static HashMap<String, Function<Double, Double>> circleFormulas = new HashMap<String, Function<Double, Double>>() {
        {
            put("DIA", (Double r) -> r * 2);
            put("AR", (Double r) -> Math.PI * (r * r));
            put("PER", (Double r) -> 2 * Math.PI * r);
            put("ARSEM", (Double r) -> Math.PI * r);
        }
    };

    @Property
    void testDiameter(@ForAll("doubles") double n){
        Assertions.assertEquals(circleFormulas.get("DIA").apply(n) ,calculateCircleData.calculateCircleData(n, "DIA"), "Input: " + n);
    }
    @Property
    void testArea(@ForAll("doubles") double n){
        Assertions.assertEquals(circleFormulas.get("AR").apply(n) ,calculateCircleData.calculateCircleData(n, "AR"), "Input: " + n);
    }
    @Property
    void testPerimeter(@ForAll("doubles") double n){
        Assertions.assertEquals(circleFormulas.get("PER").apply(n) ,calculateCircleData.calculateCircleData(n, "PER"), "Input: " + n);
    }
    @Property
    void testAreaOfSemiCircle(@ForAll("doubles") double n){
        Assertions.assertEquals(circleFormulas.get("ARSEM").apply(n) ,calculateCircleData.calculateCircleData(n, "ARSEM"), "Input: " + n);
    }

    @Provide
    Arbitrary<Double> doubles(){
        return Arbitraries.doubles().between(0,100);
    }
}
