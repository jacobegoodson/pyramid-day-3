import net.jqwik.api.*;
import org.junit.jupiter.api.Assertions;

import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.IntStream;

public class NumberPatternTest {
    private NumberPattern numberPattern = new NumberPattern();

    @Property void testNumberPattern(@ForAll("nums") int n){
        AtomicReference<String> stringTest = new AtomicReference<>("");

        if (n % 3 != 0){
            IntStream.range(0,n).forEach(num -> stringTest.updateAndGet(v -> v + n + " "));
        }

        Assertions.assertEquals(stringTest.get().trim(), numberPattern.numberPatternNthRow(n), "Input: " + n);
    }

    @Provide
    Arbitrary<Integer> nums(){
        return Arbitraries.integers().between(0,100);
    }
}
