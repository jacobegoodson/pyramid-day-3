/*
    PROBLEM STATEMENT
        Given a double radius, and a String code (code represents what the user is wanting to calculate)
        make a calculation that uses the String code and a given formula, the calculation should return a double, thus
        the method calculateCircleData should return a double. You should get input from the user and calculate based
        off of that input.

    The codes are and their corresponding formulas are:
        Diameter               "DIA"          2 r
        Area                   "AR"           π r^2 (carrot means to raise to the power of 2, or square r)
        Perimeter              "PER"          2 π r
        Area of Semi Circle    "ARSEM"        π r


    Example of method:
       calculateCircleData(2, "DIA") => 4.0
       calculateCircleData(2, "ARSEM") => (approximately) 6.283185307179586476925286766559

    Example of main method:
        <Program Start>
            Enter the radius:
            5
            Enter the code:
            DIA
            The diameter of the circle is 10
        <Program Finish>

*/

public class CalculateCircleData {

    public double calculateCircleData(double radius, String code) {
        // your code goes here
        return 0;
    }
}
